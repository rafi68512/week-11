const request = require("supertest");
const app = require("../app/index");
const sequelize = require("../config/config");
const Todo = require("../app/models/todo");

beforeAll(async () => {
  await sequelize.sync();
});

afterAll(async () => {
  await sequelize.close();
});

describe("Todo API Endpoints", () => {
  let todoId;

  // Test Create Todo
  it("should create a new todo", async () => {
    const response = await request(app)
      .post("/api/todos")
      .send({ title: "New Todo" });
    expect(response.statusCode).toEqual(201);
    expect(response.body).toHaveProperty("id");
    todoId = response.body.id;
  });

  // Test Get All Todos
  it("should get all todos", async () => {
    const response = await request(app).get("/api/todos");
    expect(response.statusCode).toEqual(200);
    expect(response.body.length).toBeGreaterThanOrEqual(1);
  });

  // Test Get Todo by ID
  it("should get a todo by ID", async () => {
    const response = await request(app).get(`/api/todos/${todoId}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body.id).toEqual(todoId);
  });

  // Test Soft Delete Todo
  it("should soft delete a todo", async () => {
    const response = await request(app).delete(`/api/todos/${todoId}`);
    expect(response.statusCode).toEqual(200);
    expect(response.body.message).toEqual("Todo deleted successfully");
  });

  // Test Get Deleted Todo by ID (Expect 404)
  it("should return 404 for deleted todo", async () => {
    const response = await request(app).get(`/api/todos/${todoId}`);
    expect(response.statusCode).toEqual(404);
  });
});
