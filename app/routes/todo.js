const express = require("express");
const router = express.Router();
const Todo = require("../models/todo");

// 1. List All Todo
router.get("/", async (req, res) => {
  try {
    const todos = await Todo.findAll({ where: { deletedAt: null } });
    res.status(200).json(todos);
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// 2. Detail Todo
router.get("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const todo = await Todo.findOne({ where: { id, deletedAt: null } });
    if (todo) {
      res.status(200).json(todo);
    } else {
      res.status(404).json({ error: "Todo not found" });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

// 3. Create Todo
router.post("/", async (req, res) => {
  const { title } = req.body;
  try {
    const todo = await Todo.create({ title });
    res.status(201).json(todo);
  } catch (error) {
    res.status(400).json({ error: error.message });
  }
});

// 4. Delete Todo (Soft Delete)
router.delete("/:id", async (req, res) => {
  const id = req.params.id;
  try {
    const todo = await Todo.findOne({ where: { id, deletedAt: null } });
    if (todo) {
      await todo.update({ deletedAt: new Date() });
      res.status(200).json({ message: "Todo deleted successfully" });
    } else {
      res.status(404).json({ error: "Todo not found" });
    }
  } catch (error) {
    res.status(500).json({ error: error.message });
  }
});

module.exports = router;
