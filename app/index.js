const express = require("express");
const bodyParser = require("body-parser");
const sequelize = require("../config/config");
const todoRoutes = require("./routes/todo");

const app = express();
const PORT = process.env.PORT || 3000;

app.use(bodyParser.json());

// Hubungkan rute Todo
app.use("/todos", todoRoutes);

// Sinkronisasi database dan jalankan server
sequelize
  .sync()
  .then(() => {
    console.log("Database connected");
    app.listen(PORT, () => {
      console.log(`Server is running on port ${PORT}`);
    });
  })
  .catch((err) => {
    console.error("Unable to connect to the database:", err);
  });

module.exports = app;
